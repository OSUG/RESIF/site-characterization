This repository contains the site condition description files produced by the ISTERRE CNRS laboratory (France)
for a number of seismic stations in the RA and FR network.
The format of this metadata is specified in the file: schema/QuakeML-SERA-1.1.xsd
